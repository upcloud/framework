package pkg

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (s *Server) handlerHealthcheck(c *gin.Context) {
	c.String(http.StatusOK, "OK")
}

func (s *Server) setupHealthcheck(config *Config) error {

	router := gin.Default()
	router.GET("/healthcheck", s.handlerHealthcheck)

	s.srvHealthcheck = &http.Server{
		Addr:    ":" + strconv.Itoa(config.HealthCheckPort),
		Handler: router,
	}

	go func() {
		if err := s.srvHealthcheck.ListenAndServe(); err != nil {
			panic(err)
		}
	}()

	return nil
}
