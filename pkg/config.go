package pkg

import "net"

var (
	DefaultRPCAddr = &net.TCPAddr{IP: net.ParseIP("127.0.0.1"), Port: 4647}
)

type Config struct {
	HttpPort        int
	HealthCheckPort int
	TelemetryPort   int
	RPCAddr         *net.TCPAddr
}

func DefaultConfig() *Config {
	return &Config{
		HttpPort:        8000,
		HealthCheckPort: 8700,
		TelemetryPort:   8800,
		RPCAddr:         DefaultRPCAddr,
	}
}
