package pkg

import (
	"net"
	"net/http"
	"net/rpc"

	"github.com/hashicorp/go-multierror"
)

type Server struct {
	Config    *Config
	Endpoints endpoints

	srvHealthcheck *http.Server
	srvTelemetry   *http.Server

	// rpc
	rpcListener net.Listener
	rpcServer   *rpc.Server
}

type endpoints struct {
	Job *Job
}

func NewServer(config *Config) *Server {
	s := &Server{
		Config:    config,
		rpcServer: rpc.NewServer(),
	}

	// register the endpoints
	s.registerEndpoints()

	// start monitoring
	err := s.setupTelemetry(config)
	if err != nil {
		panic(err)
	}

	// start healthcheck
	err = s.setupHealthcheck(config)
	if err != nil {
		panic(err)
	}

	// setup rpc
	err = s.setupRPC()
	if err != nil {
		panic(err)
	}

	// Start the RPC listeners
	go s.listenRPC()

	return s
}

func (s *Server) registerEndpoints() {
	s.Endpoints.Job = &Job{s}
}

func (s *Server) Shutdown() error {
	var result error

	// stop monitoring
	if s.srvTelemetry != nil {
		if err := s.srvTelemetry.Close(); err != nil {
			result = multierror.Append(result, err)
		}
	}

	// stop healthcheck
	if s.srvHealthcheck != nil {
		if err := s.srvHealthcheck.Close(); err != nil {
			result = multierror.Append(result, err)
		}
	}

	// stop rpc
	if s.rpcListener != nil {
		if err := s.rpcListener.Close(); err != nil {
			result = multierror.Append(result, err)
		}
	}

	return result
}
