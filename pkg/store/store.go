package store

import (
	"framework/pkg/model"
)

type Datastore interface {
	GetJob(string) (*model.Job, error)
}
