package pkg

import (
	"time"

	metrics "github.com/armon/go-metrics"
)

type Job struct {
	s *Server
}

type JobSpecificRequest struct{}

type SingleJobResponse struct {
	Num int
}

var count = 0

func (j *Job) GetJobs(args *JobSpecificRequest, reply *SingleJobResponse) error {
	defer metrics.MeasureSince([]string{"handler", "getJobs"}, time.Now())

	reply.Num = count
	count++

	return nil
}
