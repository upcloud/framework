package pkg

import (
	"net/http"
	"strconv"

	metrics "github.com/armon/go-metrics"
	"github.com/armon/go-metrics/prometheus"
	"github.com/gin-gonic/gin"
	prom "github.com/prometheus/client_golang/prometheus"
)

// Telemetry is the configuration
type Telemetry struct {
}

func (s *Server) setupTelemetry(config *Config) error {
	sink, err := prometheus.NewPrometheusSink()
	if err != nil {
		return err
	}

	_, err = metrics.NewGlobal(metrics.DefaultConfig("apiserver"), sink)
	if err != nil {
		return err
	}

	router := gin.Default()
	router.GET("/metrics", gin.WrapH(prom.Handler()))

	s.srvTelemetry = &http.Server{
		Addr:    ":" + strconv.Itoa(config.TelemetryPort),
		Handler: router,
	}

	go func() {
		if err := s.srvTelemetry.ListenAndServe(); err != nil {
			panic(err)
		}
	}()

	return nil
}
