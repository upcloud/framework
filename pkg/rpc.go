package pkg

import (
	"fmt"
	"net"
)

func (s *Server) setupRPC() error {

	fmt.Println("-- register job --")
	fmt.Println(s.Endpoints.Job)

	s.rpcServer.Register(s.Endpoints.Job)

	listener, err := net.ListenTCP("tcp", s.Config.RPCAddr)
	if err != nil {
		return err
	}

	s.rpcListener = listener
	return nil
}

func (s *Server) listenRPC() {
	for {
		conn, err := s.rpcListener.Accept()
		if err != nil {
			// log
			continue
		}

		// handle conexion
		s.rpcServer.ServeConn(conn)
	}
}
