package client_rpc

import (
	"framework/pkg"
	"net/rpc"
)

type Client struct {
	c *rpc.Client
}

func NewClient(port string) (*Client, error) {
	c, err := rpc.DialHTTP("tcp", ":"+port)
	if err != nil {
		return nil, err
	}

	client := &Client{
		c: c,
	}

	return client, nil
}

func (c *Client) GetJobs() (int, error) {
	req := pkg.JobSpecificRequest{}
	var reply pkg.SingleJobResponse

	err := c.c.Call("Job.GetJobs", req, &reply)
	if err != nil {
		return 0, err
	}

	return reply.Num, nil
}
