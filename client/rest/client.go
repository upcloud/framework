package client_http

import (
	"encoding/json"
	"framework/pkg"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/eapache/go-resiliency/breaker"
)

// interface to interact with the microservice
// in the future it can have several folders, depending on the programming
// language

// It interacts with ../pkg/handlers endpoints

type Client struct {
	Path string
}

func NewClient(ip string) *Client {
	return &Client{
		Path: ip,
	}
}

func Decode(data []byte, reply interface{}) error {
	err := json.Unmarshal(data, reply)
	return err
}

// generico de endpoints

func (c *Client) get(endpoint string, reply interface{}) error {
	b := breaker.New(3, 1, 5*time.Second)
	var resp *http.Response

	err := b.Run(func() error {
		respAux, err := http.Get(c.Path + endpoint)
		if err != nil {
			return err
		}

		resp = respAux
		return nil
	})
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err := Decode(data, reply)
}

func (c *Client) post(endpoint string, req, reply interface{}) error {
	// pasar el request como attr
	return nil
}

func (c *Client) GetJobs() (int, error) {
	var reply pkg.SingleJobResponse
	err := c.get("/jobs", &reply)

	return reply.Num, err
}

// clientA a/client
// clientA.GetJobs()

// implementar un circuit breaker
