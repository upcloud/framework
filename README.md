Microservices framework
===================

Microservices is the new cool kid on the block. There is still not much documentation of practical cases for his implementation. This example is intended to be used as a frame of reference to write efficiently and orderly a microservice in Golang.

(Documentation is a working in progress)

----------

### Architecture

Folder structure:

```
/client
	/rest
		/rest_client.go
/vendor
/api
	/http.go
	/endpoints.go
/pkg
	/model
		/entities.go
	/store
		/store.go
		/postgresql.go
	/..._handler.go
	/config.go
	/health.go
	/monitor.go
	/server.go
/cmd
	/root.go
/main.go
```

#### cmd

The entry point of the microservice is the cmd package written with spf13/cobra. Each of the functionality of the service can be written as a cli command.

#### pkg

Stores the main logic of the microservice. 

##### config.go

##### health.go

Creates an http endpoint that returns 200 whenever the service is running.

##### monitor.go

The microservice uses armon/go-metrics to register the metrics. Monitor.go initialize the service and uses prometheus to expose the metrics. The library can use citronous and datadog as well.

##### model

The entities of the microservice

#### store

Store.go is an interface with all the possible functions on the store. Postgresql.go implements the store with PostgreSQL.

##### server.go

The main entry point of the microservice. It creates the monitoring, healthcheck, store....

##### ..._handlers

The handlers are the exposed endpoints of the microservice. The handle all the internal logic. 

#### API

It creates an HTTP server and expose the endpoints in ..._handlers as an http API.

#### Client

The library clients stores the libraries to interconnect with the microservice. 