package http

import (
	"framework/pkg"
	"strconv"

	"fmt"

	"github.com/gin-gonic/gin"
)

type Http struct {
	s *pkg.Server
	r *gin.Engine
}

func NewHttp(s *pkg.Server) *Http {
	h := &Http{
		s: s,
		r: gin.Default(),
	}

	h.registerEndpoints()
	return h
}

func (h *Http) registerEndpoints() {
	h.r.GET("/jobs", h.GetJobs)
}

func (h *Http) Serve() {
	fmt.Printf("Running http server at %d\n", h.s.Config.HttpPort)
	h.r.Run(":" + strconv.Itoa(h.s.Config.HttpPort))
}

func (h *Http) Shutdown() {

}
