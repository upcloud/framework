package http

import (
	"framework/pkg"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h *Http) GetJobs(c *gin.Context) {
	req := pkg.JobSpecificRequest{}
	var reply pkg.SingleJobResponse

	err := h.s.Endpoints.Job.GetJobs(&req, &reply)
	if err != nil {
		c.String(http.StatusInternalServerError, "OK")
	}

	c.JSON(http.StatusOK, reply.Num)
}
