package cmd

import (
	"fmt"
	pkgHttp "framework/http"
	"framework/pkg"

	rpcClient "framework/client/rpc"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(runCmd)
	RootCmd.AddCommand(queryCmd)
}

var RootCmd = &cobra.Command{
	Use:   "microservice",
	Short: "Hugo is a very fast static site generator",
	Long:  `A Fast and Flexible Static Site Generator built with`,
	Run: func(cmd *cobra.Command, args []string) {

	},
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Print the version number of Hugo",
	Long:  `All software has versions. This is Hugo's`,
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here

		fmt.Println(pkg.DefaultConfig())

		s := pkg.NewServer(pkg.DefaultConfig())

		fmt.Println(s)

		httpServer := pkgHttp.NewHttp(s)
		httpServer.Serve()

		done := make(chan bool)
		<-done
	},
}

var queryCmd = &cobra.Command{
	Use:   "query",
	Short: "Print the version number of Hugo",
	Long:  `All software has versions. This is Hugo's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("query call")

		c, err := rpcClient.NewClient("4647")
		if err != nil {
			panic(err)
		}

		fmt.Println(c.GetJobs())

	},
}
